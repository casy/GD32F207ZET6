
#include "bsp_led.h"

/*************************************************************************************************
 *  功能：初始化用户Led设备                                                                      *
 *  参数：无                                                                                     *
 *  返回：无                                                                                     *
 *  说明：                                                                                       *
 *************************************************************************************************/
void BSP_Led_Config(void)
{
	/* enable the led clock */
	rcu_periph_clock_enable(RCU_GPIOD);
	/* configure led GPIO port */ 
	gpio_init(GPIOD, GPIO_MODE_OUT_PP, GPIO_OSPEED_50MHZ, GPIO_PIN_11 | GPIO_PIN_12 |GPIO_PIN_13);
	gpio_bit_reset(GPIOD, GPIO_PIN_11 | GPIO_PIN_12 |GPIO_PIN_13);
}


/*************************************************************************************************
 *  功能：控制Led的点亮和熄灭                                                                    *
 *  参数：(1) index Led灯编号                                                                    *
 *        (2) cmd   Led灯点亮或者熄灭的命令                                                      *
 *  返回：无                                                                                     *
 *  说明：                                                                                       *
 *************************************************************************************************/
void BSP_Led_Control(led_type led, int cmd)
{
    switch (led)
    {
        case LED_red:
        {
            if(cmd == LED_ON)
            {
                gpio_bit_set(GPIOD,GPIO_PIN_11);  /*点亮Led1灯*/
            }
            else if(cmd == LED_OFF)
            {
                gpio_bit_reset(GPIOD,GPIO_PIN_11);  /*熄灭Led1灯*/
            }
			else
			{
				  gpio_bit_write(GPIOD, GPIO_PIN_11, (bit_status)(1U-gpio_input_bit_get(GPIOD, GPIO_PIN_11)));
			}
            break;
        }
        case LED_green:
        {
            if(cmd == LED_ON)
            {
                gpio_bit_set(GPIOD,GPIO_PIN_12);  /*点亮Led2灯*/
            }
            else if(cmd == LED_OFF)
            {
                gpio_bit_reset(GPIOD,GPIO_PIN_12);  /*熄灭Led2灯*/
            }
			else
			{
				gpio_bit_write(GPIOD, GPIO_PIN_12, (bit_status)(1U-gpio_input_bit_get(GPIOD, GPIO_PIN_12)));
			}
            break;
        }
        case LED_yellow:
        {
            if(cmd == LED_ON)
            {
                gpio_bit_set(GPIOD,GPIO_PIN_13);  /*点亮Led3灯*/
            }
            else if(cmd == LED_OFF)
            {
                gpio_bit_reset(GPIOD,GPIO_PIN_13);  /*熄灭Led3灯*/
            }
			else
			{
				gpio_bit_write(GPIOD, GPIO_PIN_13, (bit_status)(1U-gpio_input_bit_get(GPIOD, GPIO_PIN_13)));
			}
            break;
        }
        default:     
            break;
    }
}

