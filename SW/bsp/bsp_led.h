#ifndef __BSP_LED_H
#define __BSP_LED_H

#ifdef cplusplus
 extern "C" {
#endif

#include "gd32f20x_gpio.h"


/* exported types */
typedef enum 
{
    LED_red = 0,
    LED_green = 1,
    LED_yellow = 2,
} led_type;


#define LED_OFF 	(0)
#define LED_ON  	(1)
#define LED_TOGGLE  (2)


void BSP_Led_Config(void);
void BSP_Led_Control(led_type led, int cmd);


#ifdef cplusplus
}
#endif

#endif 
