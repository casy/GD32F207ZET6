/*!
    \file  main.c
    \brief led spark with systick, USART print and key example

    \version 2015-07-15, V1.0.0, firmware for GD32F20x
    \version 2017-06-05, V2.0.0, firmware for GD32F20x
    \version 2018-10-31, V2.1.0, firmware for GD32F20x
*/


#include "main.h"


int main(void)
{
    /* configure systick */
	systick_config();
	
	nvic_priority_group_set(NVIC_PRIGROUP_PRE0_SUB4);
	
	
	BSP_Led_Config();
	
	Uartx_Config(USART0,115200); /*��Ӧ����CH340*/
	
	/* print out the clock frequency of system, AHB, APB1 and APB2 */
    printf("\r\nCK_SYS is %dHz", rcu_clock_freq_get(CK_SYS));
    printf("\r\nCK_AHB is %dHz", rcu_clock_freq_get(CK_AHB));
    printf("\r\nCK_APB1 is %dHz", rcu_clock_freq_get(CK_APB1));
    printf("\r\nCK_APB2 is %dHz", rcu_clock_freq_get(CK_APB2));

	
		while(1)
		{
				delay_1ms(300);
				BSP_Led_Control(LED_green,LED_ON);
				delay_1ms(300);
				BSP_Led_Control(LED_green,LED_OFF);
				delay_1ms(500);
				BSP_Led_Control(LED_red,LED_OFF);
				BSP_Led_Control(LED_yellow,LED_ON);
				delay_1ms(500);
				BSP_Led_Control(LED_red,LED_ON);
				BSP_Led_Control(LED_yellow,LED_OFF);
			  		
				printf("\r\nThis is a usart transmit test example!");
		}

#if 0	
    /* initilize the LEDs, USART and key */
   
    gd_eval_com_init(EVAL_COM1);
    gd_eval_key_init(KEY_WAKEUP, KEY_MODE_GPIO);
    
  
    while(1){
        if(RESET == gd_eval_key_state_get(KEY_WAKEUP)){
            gd_eval_led_on(LED3);
            delay_1ms(500);
            gd_eval_led_off(LED3);
            gd_eval_led_toggle(LED4);
        }
    }
		
#endif

}

