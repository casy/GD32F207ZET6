
#include "uart.h"


/**
  * @brief  Configure the nested vectored interrupt controller.
  * @param  None
  * @retval None
  */
void Uartx_NVIC_Config(void)
{

	
}



/* retarget the C library printf function to the USART */
int fputc(int ch, FILE *f)
{
    usart_data_transmit(DEBUG_UART, (uint8_t)ch);
    while(RESET == usart_flag_get(DEBUG_UART, USART_FLAG_TBE));
    return ch;
}

/*
configure COM1 port hardward connect: 
PA9 ---- TX
PA10 ---- RX

configure COM0 port hardward connect: 
PB6 -- TX
PB7 -- RX
*/
void Uartx_Config(uint32_t usart_periph, uint32_t baudrate)
{ 
	uint32_t COMx = usart_periph;
	
	if(COMx == USART0) 
	{
			/* enable GPIO clock */
			rcu_periph_clock_enable(RCU_GPIOB);
			/* enable USART clock */
			rcu_periph_clock_enable(RCU_USART0);
			rcu_periph_clock_enable(RCU_AF);

			/* connect port to USARTx_Tx */
			gpio_init(GPIOB, GPIO_MODE_AF_PP, GPIO_OSPEED_50MHZ, GPIO_PIN_6);
			/* connect port to USARTx_Rx */
			gpio_init(GPIOB, GPIO_MODE_IN_FLOATING, GPIO_OSPEED_50MHZ, GPIO_PIN_7);
				
			gpio_pin_remap_config(GPIO_USART0_REMAP,ENABLE);
		
			
	}

    /* USART configure */
    usart_deinit(COMx);
    usart_baudrate_set(COMx, baudrate);
    usart_word_length_set(COMx, USART_WL_8BIT);
    usart_stop_bit_set(COMx, USART_STB_1BIT);
    usart_parity_config(COMx, USART_PM_NONE);
    usart_hardware_flow_rts_config(COMx, USART_RTS_DISABLE);
    usart_hardware_flow_cts_config(COMx, USART_CTS_DISABLE);
    usart_receive_config(COMx, USART_RECEIVE_ENABLE);
    usart_transmit_config(COMx, USART_TRANSMIT_ENABLE);
    usart_enable(COMx);
	
	/* Enable the USARTx Receive interrupt */
	usart_interrupt_enable(COMx, USART_INT_RBNE);
	
	if(COMx == USART0) 
	{
		nvic_irq_enable(USART0_IRQn, 0, 1);	
	}

}


/*************************************************************************************************
 *  功能：向串口1发送一个字符                                                                    *
 *  参数：(1) 需要被发送的字符                                                                   *
 *  返回：                                                                                       *
 *  说明：                                                                                       *
 *************************************************************************************************/
void UartxWriteByte(uint32_t usart_periph,char c)
{
   	uint32_t COMx = usart_periph;		
	usart_data_transmit(COMx, c);
	while (usart_flag_get(COMx, USART_FLAG_TBE) == RESET);		
}


/*************************************************************************************************
 *  功能：向串口1发送一个字符串                                                                  *
 *  参数：(1) 需要被发送的字符串                                                                 *
 *  返回：                                                                                       *
 *  说明：                                                                                       *
 *************************************************************************************************/
void UartxWriteStr(uint32_t usart_periph, const char* str)
{
  	uint32_t COMx = usart_periph;	
	while (*str)
    {
        usart_data_transmit(COMx, *str++);
        while (usart_flag_get(COMx, USART_FLAG_TBE) == RESET) ;
    }
}

uint32_t rxcount = 0;
uint32_t rxbuffer[200];

void USART0_IRQHandler(void)
{
		uint8_t i  = 0;
		if(RESET != usart_interrupt_flag_get(USART0, USART_INT_FLAG_RBNE))
		{
			/* receive data OK*/
			rxbuffer[rxcount++] = usart_data_receive(USART0);
       
		}
}



void USART1_IRQHandler(void)
{
		uint8_t i  = 0;
		if(SET == usart_interrupt_flag_get(USART1, USART_INT_FLAG_RBNE))
		{
			i++;
		
		}
}



#if 0


*/
void USART0_IRQHandler(void)
{
    if(RESET != usart_interrupt_flag_get(USART0, USART_INT_FLAG_RBNE)){
        /* receive data */
        rxbuffer[rxcount++] = usart_data_receive(USART0);
        if(rxcount == rx_size){
            usart_interrupt_disable(USART0, USART_INT_RBNE);
        }
    }
    if(RESET != usart_interrupt_flag_get(USART0, USART_INT_FLAG_TBE)){
        /* transmit data */
        usart_data_transmit(USART0, txbuffer[txcount++]);
        if(txcount == tx_size){
            usart_interrupt_disable(USART0, USART_INT_TBE);
        }
    }
}



/*************************************************************************************************
 *  功能：从串口1接收一个字符                                                                    *
 *  参数：(1) 存储接收到的字符                                                                   *
 *  返回：                                                                                       *
 *  说明：                                                                                       *
 *************************************************************************************************/
void EvbUart1ReadByte(char* c)
{
    while (USART_GetBitState(USART1, USART_FLAG_RBNE) == RESET)
        ;
    *c = (USART_DataReceive(USART1));
}


static char _buffer[256];
void EvbUart1Printf(char* fmt, ...)
{
    int i;
    va_list ap;
    va_start(ap, fmt);
    vsprintf(_buffer, fmt, ap);
    va_end(ap);

    for (i = 0; _buffer[i] != '\0'; i++)
    {
        EvbUart1WriteByte(_buffer[i]);
    }
}

void EvbUart1EnableRxInt(void)
{
    /* Enable the USART1 Receive interrupt */
    USART_INT_Set( USART1, USART_INT_RBNE, ENABLE );

}

void EvbUart1EnableTxInt(void)
{
    /* Enable the USART1 Transmoit interrupt */
    USART_INT_Set( USART1, USART_INT_TBE, ENABLE );
}

#endif

#if 0
void EvbUart1ISR(void)
{
    if( USART_GetIntBitState(USART1, USART_INT_RBNE) != RESET)
    {
        /* Read one byte from the receive data register */
        RxBuffer[ RxCounter++ ] = ( uint8_t )USART_DataReceive( USART1 );

        if( RxCounter >= NbrOfDataToRead )
        {
            /* Disable the USARTy Receive interrupt */
            USART_INT_Set( USART1 , USART_INT_RBNE , DISABLE );
        }

    }


    if( USART_GetIntBitState( USART1, USART_INT_TBE ) != RESET )
    {
        /* Write one byte to the transmit data register */
        USART_DataSend( USART1 , TxBuffer[ TxCounter ++ ] );

        if( TxCounter >= NbrOfDataToSend )
        {
            /* Disable the USART1 Transmit interrupt */
            USART_INT_Set(USART1, USART_INT_TBE, DISABLE);
        }
    }

}
#endif
