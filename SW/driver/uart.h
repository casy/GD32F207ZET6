#ifndef __UART_H
#define __UART_H

#include "gd32f20x_usart.h"

#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>


void Uartx_NVIC_Config(void);

void Uartx_Config(uint32_t usart_periph, uint32_t baudrate);
	
void UartxWriteByte(uint32_t usart_periph,char c);

void UartxWriteStr(uint32_t usart_periph, const char* str);


#define DEBUG_UART  USART0   /*定义调试printf打印串口*/


#endif /* _UART_H */
